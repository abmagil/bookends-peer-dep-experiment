const semver = require('semver');
const chalk = require('chalk');

const pkgJson = require('./package.json');

const checkForPackage = (pkgName, requiredVersionRange) => {
  try {
    const pkgVersion = require(`${pkgName}/package.json`).version;
    return semver.satisfies(pkgVersion, requiredVersionRange)
  }
  catch (e) {
    return false;
  }
}

const { peerDependencies, version } = pkgJson;
const isMissingAnyPackage = Object.entries(peerDependencies).find(peerDep => {
  return !checkForPackage(...peerDep);
})


isMissingAnyPackage && console.log(chalk.bgBlue(`\nBookends v${version} requires the packages declared as peerDependencies to behave correctly.\n\nPlease install peer dependencies\n`));
