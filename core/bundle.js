import "./styles.css"

/*
* browser classes
* used to allow browser-specific CSS
*/

var myUserAgent = navigator.userAgent;
var docElement = document.documentElement; // the <html> tag

if ((myUserAgent.indexOf("Firefox/") !== -1) && (myUserAgent.indexOf("Seamonkey/") == -1)) {
    docElement.classList.add("firefox");
}

if ((myUserAgent.indexOf("Trident/") !== -1)) {
    docElement.classList.add("ie11"); // this gets put on any version of IE, but hey, IE11 is the only version we support
}

if ((myUserAgent.indexOf("Edge/") !== -1)) {
    docElement.classList.add("edge"); 
}

/*
* accessibility-keyboard-clickable
* On elements with this class, keypresses for Enter or Space get turned into clicks
* (also support other classes that need this feature as part of their functionality, such as collapsible headers)
*/
var simulateClick = function (elem) {
    var evt = new MouseEvent("click", {
        bubbles: true,
        cancelable: true,
        view: window
    });
    elem.dispatchEvent(evt);
};

document.addEventListener("keypress", function (event) {
    var clickedElement = event.target;
    var clickedElementClassList = clickedElement.classList;

    if (clickedElementClassList.contains("accessibility-keyboard-clickable") || clickedElementClassList.contains("collapsible-header")) {
        if (event.key == "Enter" || event.key == " " || event.key == "Spacebar") // IE11 uses "Spacebar" instead of " "
        {
            simulateClick(clickedElement);
        }
    }
});

/*
* input with floating label - polyfill for IE/Edge
* (these browsers don't support :placeholder-shown)
*/
function browserNeedsInputWithFloatingLabelPolyfill() {
    var htmlTagClassList = document.querySelector("html").classList;
    return (htmlTagClassList.contains("ie11") || htmlTagClassList.contains("edge"));
}

function _inputWithFloatingLabelPolyfillCheckEmpty(elm) {
    if (elm.value !== "")
    {
        elm.classList.add("polyfill-nonempty");
    }
    else
    {
        elm.classList.remove("polyfill-nonempty");
    }
}

function inputWithFloatingLabelInit(elm) {
    if (browserNeedsInputWithFloatingLabelPolyfill()) {
        _inputWithFloatingLabelPolyfillCheckEmpty(elm);
    }
}

function _inputWithFloatingLabelDOMReady(elm) {
    if (browserNeedsInputWithFloatingLabelPolyfill()) {
        var inputs = document.querySelectorAll(".input-with-floating-label .stylized-input");

        Array.prototype.forEach.call(inputs, function (thisInput) {
            inputWithFloatingLabelInit(thisInput);
        });
    }
}

if (document.readyState !== "loading") {
    _inputWithFloatingLabelDOMReady();
} else {
    document.addEventListener("DOMContentLoaded", _inputWithFloatingLabelDOMReady);
}

document.addEventListener("change", function (event) {
    if (browserNeedsInputWithFloatingLabelPolyfill()) {

        var clickedElement = event.target;
        var parentElement = clickedElement.parentElement;

        if (parentElement && parentElement.classList.contains("input-with-floating-label"))
        {
            _inputWithFloatingLabelPolyfillCheckEmpty(clickedElement);
        }
    }
});