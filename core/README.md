# Core UI Library

This package includes the contents, CDN references, and `head` tag information for the Vistaprint UI Library's `core`
submodule.

## Consuming This Package
### Contents
The CSS and Javascript contents of the `core` submodule of the UI Library are available from `bundle.js`. We recommend
importing this file in your app as part of a webpack-based build step. In so doing, the styles will also be bundled, so
some form of CSS loader is required.

### CDN Links
The URLs for the CDN-provided CSS and Javascript files for the `core` submodule are available from `cdn.js`. By
importing only this file, the final bundle size of your app will be significantly reduced. We recommend importing them
and including them directly into your app. For instance, with React and React-Helmet, they could be consumed as follows:
```javascript
const { Helmet } = require("react-helmet")
const { css, js } = require('@hopper/ui-lib-core-dependency/cdn')

const MyApp = () => (
  <Helmet>
    <link
      rel="stylesheet"
      type="text/css"
      href={css}
    />
  </Helmet>
  <script defer type="text/javascript" src={js}></script>,
)
```

### Head Tag Data
The Head Tag Data is specific to the UI Library Core submodule and does not apply to any other UI Library submodule. It
must be converted from a JSON data structure to JSX or HTML. The interface for this structure is:
```typescript
{
  metaTags: Array<{[key]: value}>,
  linkTags: Array<{[key]: value}>,
  styleTags: Array<{
    [key: string]: value,
    contents: {
      fontFaces: Array<string>
    }
  }>
}
```

The `metaTags` and `linkTags` arrays can be mapped into tags of their respective type (`meta` or `link` respectively)
where the attributes on each tag are the Key-Value pairs of the element.

The `styleTags` array must be handled differently, where, for each element, the `style` tag has the same key-value pair
behavior (becoming HTML attributes) but the `contents` key represents the `innerHTML` of a given `<style>` node.