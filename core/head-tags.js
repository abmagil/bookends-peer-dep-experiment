export default {
  metaTags: [
    { name: "viewport", content: "initial-scale=1" }
  ],
  linkTags: [
    { rel: "shortcut icon", href: "https://ui-library.cdn.vpsvc.com/images/favicon/favicon.ico" },
    { rel: "apple-touch-icon", sizes: "152x152", href: "https://ui-library.cdn.vpsvc.com/images/favicon/vistaprint-favorites-76-76-2014-2x.png" },
    { rel: "apple-touch-icon", sizes: "120x120", href: "https://ui-library.cdn.vpsvc.com/images/favicon/vistaprint-favorites-60-60-2014-2x.png" },
    { rel: "apple-touch-icon", sizes: "76x76", href: "https://ui-library.cdn.vpsvc.com/images/favicon/vistaprint-favorites-76-76-2014.png" },
    { rel: "apple-touch-icon", sizes: "60x60", href: "https://ui-library.cdn.vpsvc.com/images/favicon/vistaprint-favorites-60-60-2014.png" },
    { rel: "preload", href: "https://ui-library.cdn.vpsvc.com/fonts/MarkWeb-Light_modified.woff", as: "font", crossorigin: "anonymous" },
    { rel: "preload", href: "https://ui-library.cdn.vpsvc.com/fonts/MarkWeb-Bold_modified.woff", as: "font", crossorigin: "anonymous" },
    { rel: "preload", href: "https://ui-library.cdn.vpsvc.com/fonts/MarkWeb-Medium_modified.woff", as: "font", crossorigin: "anonymous" },
  ],
  styleTags: [
    {
      type: "text/css",
      contents: [
        { "@font-face": `font-family: 'MarkPro'; src: url("https://ui-library.cdn.vpsvc.com/fonts/MarkWeb-Light_modified.woff") format("woff");` },
        { "@font-face": `font-family: 'MarkPro'; src: url("https://ui-library.cdn.vpsvc.com/fonts/MarkWeb-Bold_modified.woff") format("woff"); font-weight: bold` },
        { "@font-face": `font-family: 'MarkPro'; src: url("https://ui-library.cdn.vpsvc.com/fonts/MarkWeb-Medium_modified.woff") format("woff"); font-weight: 600` },
      ]
    }
  ]
}