import "./styles.css"

/*
 * Stylized Dialog
 */

// if the browser does not natively support <dialog>, load the polyfill from the CDN
if (window.HTMLDialogElement === undefined) {
  var newScript = document.createElement("script");
  newScript.src = "https://ui-library.cdn.vpsvc.com/js/thirdparty/dialog/dialog-polyfill_min.js";
  document.body.appendChild(newScript);
}

function showStylizedDialog(thisDialog)
{
  if (thisDialog.classList.contains("stylized-dialog") && !(thisDialog.hasAttribute("open"))) {

      var thisDialogClassList = thisDialog.classList;
      
      if (typeof dialogPolyfill != "undefined") {
          dialogPolyfill.registerDialog(thisDialog); // browsers that use the polyfill need the dialogs to be explicitly registered before they will work
          document.body.appendChild(thisDialog); // browsers that use the polyfill need the dialog to be a direct child of the body, for positioning reasons        
      }
      thisDialogClassList.add("typography-2017"); // since the dialogs may be direct children of the body tag, dialogs won't inherit the modern typography from a parent element, so let's add it
      thisDialog.showModal(); // invoke native method to show the dialog, but it will be visually hidden from the user until we call _makeStylizedDialogVisible()

      // if the dialog does not already have a close button, add one -- unless this dialog isn't supposed to have one
      if (!(thisDialog.querySelector(".stylized-dialog-close-button")) && !(thisDialogClassList.contains("stylized-dialog-no-close-button"))) {
          var closeButton = document.createElement("button");
          closeButton.classList.add("stylized-dialog-close-button");
          thisDialog.insertBefore(closeButton, thisDialog.firstChild); 
      }

      // set max width
      var maxWidth = thisDialog.getAttribute("data-dialog-max-width");
      if (maxWidth) {
          if (!(isNaN(maxWidth)))
          {
              // maxWidth is a number, so it has no unit, thus we add one
              maxWidth = maxWidth + "px";
          }
          thisDialog.style.maxWidth = maxWidth;
      }

      // clicks to the backdrop (meaning clicks outside the dialog) close the dialog
      thisDialog.addEventListener("click", _detectStylizedDialogBackgroundClick);

      // when the dialog is closed or cancelled, remove classes we no longer want, and remove the preloader (if present)
      
      thisDialog.addEventListener("close", _dialogClosedOrCancelled);
      thisDialog.addEventListener("cancel", _dialogClosedOrCancelled);

      // if an iframe dialog, create the iframe; otherwise, show the dialog
      var iframeURL = thisDialog.getAttribute("data-dialog-iframe-url");
      if (iframeURL) // iframe dialog
      {
          // add the preloader to the page
          var preloaderGraphic = document.createElement("aside");
          preloaderGraphic.classList.add("preloader-graphic"); // IE11 won't take multiple arguments for add()
          preloaderGraphic.classList.add("preloader-graphic-large"); // IE11 won't take multiple arguments for add()
          preloaderGraphic.setAttribute("aria-live", "polite");
          preloaderGraphic.setAttribute("role", "status");

          var preloaderGraphicInner = document.createElement("span");
          preloaderGraphicInner.classList.add("preloader-graphic-inner");
          preloaderGraphic.appendChild(preloaderGraphicInner);

          var dialogPreloader = document.createElement("aside");
          dialogPreloader.classList.add("stylized-dialog-preloader");
          dialogPreloader.appendChild(preloaderGraphic);
          document.body.appendChild(dialogPreloader);

          // if the iframe doesn't already exist, create it
          if (thisDialog.querySelector(".stylized-dialog-iframe") === null) {              
              var iframe = document.createElement("iframe");
              iframe.setAttribute("src", iframeURL);
              iframe.classList.add("stylized-dialog-iframe");
              iframe.onload = _stylizedDialogIframeLoaded;
              var iframeId = thisDialog.getAttribute("data-dialog-iframe-id");
              if (iframeId) {
                  iframe.id = iframeId;
              }
              var iframeTitle = thisDialog.getAttribute("data-dialog-iframe-title");
              if (iframeTitle) {
                  iframe.title = iframeTitle;
              }
              thisDialog.appendChild(iframe);
          }
      }
      else // not an iframe dialog
      {
          _makeStylizedDialogVisible(thisDialog);
      }
  }

  return;
}

function _detectStylizedDialogBackgroundClick(thisEvent)
{
  var thisDialog = this;

  // ignore clicks that were triggered programmatically
  // we assume a click was programmatic if the screen-position of the click is recorded as 0,0
  // we believe this is safe because a non-programmatic click at screen-position 0,0 is _highly_ unlikely (maybe impossible)
  if (thisEvent.screenX === 0 && thisEvent.screenY === 0) {
      return;
  }

  // clicks to the backdrop (meaning clicks outside the dialog) close the dialog
  var rect = thisDialog.getBoundingClientRect();
  var clickedAbove = rect.top > thisEvent.clientY;
  var clickedBelow = thisEvent.clientY > rect.top + rect.height;
  var clickedToLeft = rect.left > thisEvent.clientX;
  var clickedToRight = thisEvent.clientX > rect.left + rect.width;

  var clickIsOutsideDialog = clickedAbove || clickedBelow || clickedToLeft || clickedToRight;

  if (clickIsOutsideDialog) {
      closeStylizedDialog(thisDialog);
  }
}

function _dialogClosedOrCancelled()
{
  var thisDialogClassList = this.classList;
  
  _removeStylizedDialogBodyConstraint();

  thisDialogClassList.remove("stylized-dialog-visible"); // IE11 won't take multiple arguments for remove()
  thisDialogClassList.remove("stylized-dialog-scrollable");  // IE11 won't take multiple arguments for remove()
  
  _removeStylizedDialogPreloader();
}

function _makeStylizedDialogVisible(thisDialog)
{
  // put the close button (or, lacking that, the first active element) in focus when it opens, per WCAG
  var closeButton = thisDialog.querySelector('.stylized-dialog-close-button');
  if (closeButton)
  {
      closeButton.focus();
  }
  else
  {
      var firstFocusableElement = thisDialog.querySelector("input, button, select, textarea, .textbutton");
      if (firstFocusableElement) {
          firstFocusableElement.focus();
      }
  }

  var thisDialogClassList = thisDialog.classList;

  _addStylizedDialogBodyConstraint();

  // if the dialog is taller than 90% of the screen height, or is a takeover or panel, make it so the user can scroll to see the whole dialog.
  if ((thisDialog.scrollHeight) > (window.innerHeight * 0.9) || thisDialogClassList.contains("stylized-dialog-takeover") || thisDialogClassList.contains("stylized-dialog-panel")) {
      thisDialogClassList.add("stylized-dialog-scrollable");

      // don't move the close button if this is a panel dialog
      if (!(thisDialogClassList.contains("stylized-dialog-panel")) && !(thisDialogClassList.contains("stylized-dialog-no-close-button")))
      {
          // if there is a dialog header, move the close button to the inside of the header. This keeps the close button "sticky" along with the rest of the header
          var dialogHeader = thisDialog.querySelector(".stylized-dialog-header");
          if (dialogHeader && closeButton) {
              dialogHeader.insertBefore(closeButton, dialogHeader.firstChild); 
          }
      }
  }

  // showModal() does not natively trigger an event, so we create one.
  // we're using this particular syntax because "var showEvent = new CustomEvent()" doesn't work in IE11.
  // Once we drop IE11, we can replace this with that simpler syntax.
  var showEvent = document.createEvent("CustomEvent");
  showEvent.initCustomEvent("showStylizedDialog", true, true, { dialogId: thisDialog.id });
  thisDialog.dispatchEvent(showEvent);

  // adding this class will transition the dialog from transparent to opaque
  thisDialog.classList.add("stylized-dialog-visible"); 

  return;
}

// for IE11 and Edge 12-14, which only support a prefixed matches() method
if (!Element.prototype.matches) {
  Element.prototype.matches = Element.prototype.msMatchesSelector;
}

// IE11 doesn't support a native .closest() method, so we use this instead.
// When we drop IE11, we can get rid of this function, and refactor places that use it to use .closest() instead.
function _findClosestDialog(el) 
{
  while (el)
  {
      if (el.matches(".stylized-dialog"))
      {
          return el;
      }
      else
      {
          el = el.parentElement;
      } 
  }

  return null;
}

// remove the preloader
function _removeStylizedDialogPreloader()
{
  var preloaders = document.querySelectorAll(".stylized-dialog-preloader");
  if (preloaders)
  {
      for (var i = 0; i < preloaders.length; ++i)
      {
          preloaders[i].parentNode.removeChild(preloaders[i]);
      }
  }
}

// constrain the body tag and adjust its top and width values, to prevent the underlying page from scrolling while a dialog is open
function _addStylizedDialogBodyConstraint()
{
  // compute the width of the scrollbar for this browser by creating a div, measuring it, then removing it
  var scrollDiv = document.createElement("div");
  scrollDiv.className = "stylized-dialog-scrollbar-measure";
  document.body.appendChild(scrollDiv);
  var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
  document.body.removeChild(scrollDiv);

  document.body.style.top = "-" + window.pageYOffset + "px";
  document.body.style.width = "calc(100% - " + scrollbarWidth + "px)";
  document.body.classList.add("stylized-dialog-body-constrain");
}

function _removeStylizedDialogBodyConstraint()
{
  if (document.body.classList.contains("stylized-dialog-body-constrain")) {
      var currentBodyTop = document.body.style.top;
      document.body.style.top = "";
      document.body.style.width = "";
      document.body.classList.remove("stylized-dialog-body-constrain");
      // scroll the window back down by the amount the body tag had been offset
      window.scrollTo(0, parseInt(currentBodyTop) * -1);
  }
}

// once the dialog's iframe has loaded, size the iframe and display the dialog
function _stylizedDialogIframeLoaded() {
  var thisContentDocumentElement = this.contentDocument.documentElement;
  this.style.minHeight = thisContentDocumentElement.clientHeight + "px";
  this.style.minWidth = thisContentDocumentElement.clientWidth + "px";

  _makeStylizedDialogVisible(_findClosestDialog(this)); 
  _removeStylizedDialogPreloader();
}

// closes the dialog. The second argument is optional; it passes an arbitrary string value to the dialog's returnValue property
function closeStylizedDialog(thisDialog, val)
{
  _removeStylizedDialogBodyConstraint();

  thisDialog.close(val); 
  thisDialog.removeEventListener("click", _detectStylizedDialogBackgroundClick, false);
  thisDialog.removeEventListener("close", _dialogClosedOrCancelled);
  thisDialog.removeEventListener("cancel", _dialogClosedOrCancelled);
}

// close the dialog when its iframe posts a "close the dialog" message 
window.addEventListener("message", function (e) {
  if (e.data == "stylizedDialogClose") {
      // figure out which iframe sent the message
      var iframes = document.getElementsByTagName('iframe');
      var thisIframe = null;
      for (var i = 0; i < iframes.length; i++) {
          if (iframes[i].contentWindow === e.source) {
              thisIframe = iframes[i];
          }
      }

      closeStylizedDialog(_findClosestDialog(thisIframe));
  }
});

// click handlers that bubble up to the document element
document.addEventListener('click', function (event) {
  var clickedElement = event.target;

  // close the dialog when a "close" button inside it is clicked
  var clickedElementClassList = clickedElement.classList;
  if (clickedElementClassList && (clickedElementClassList.contains("stylized-dialog-close") || clickedElementClassList.contains("stylized-dialog-close-button")))
  {
      var thisDialog = _findClosestDialog(clickedElement);

      if (thisDialog) {
          // this button has an ancestor that's a dialog, so close that dialog
          closeStylizedDialog(thisDialog);
      }
      else {
          // this button doesn't have an ancestor that's a dialog, so we assume we are inside an iframe, and tell the parent window to close the dialog
          window.parent.postMessage("stylizedDialogClose", "*"); // we use * because we don't know the parent window's URL
      }
  }

  // if the element has a "data-dialog-show" attribute, show the dialog with that ID
  var dataDialogShow = clickedElement.getAttribute('data-dialog-show');
  if (dataDialogShow)
  {
      showStylizedDialog(document.getElementById(dataDialogShow));
  }
});

export default ""