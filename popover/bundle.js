import "./styles.css"

/*
 * originally from
 * https://gist.github.com/elclanrs/bb719c17504e5d9b3ec985def050a041
 * transpiled to ES5, then cleaned up and modified quite a bit
 * 
 * this script is still gross under the hood, but we need a popover component RIGHT NOW, so we'll have to clean it up later.
 * in particular, it's got Babel's transpilation functions in it; these should be reworked.
 * 
 * -Jeff Yaus, 2019 May 15
 */

// begin Babel transpilation functions
function _defineProperties(target, props) { 
  for (var i = 0; i < props.length; i++) { 
      var descriptor = props[i]; 
      descriptor.enumerable = descriptor.enumerable || false; 
      descriptor.configurable = true; 
      if ("value" in descriptor) {descriptor.writable = true;} 
      Object.defineProperty(target, descriptor.key, descriptor); 
  } 
}

function _createClass(Constructor, protoProps, staticProps) { 
  if (protoProps) {
      _defineProperties(Constructor.prototype, protoProps); 
  }
  if (staticProps) {
      _defineProperties(Constructor, staticProps); 
  }
  return Constructor; 
}
// end Babel transpilation functions


// IE11 doesn't support a native .closest() method, so we use these instead.
// When we drop IE11, we can get rid of these functions, and refactor places that use them to use .closest() instead.
function _findClosestPopover(el) {
  while (el) {
      if (el.matches(".popover")) {
          return el;
      }
      else {
          el = el.parentElement;
      }
  }

  return null;
}

function _findClosestPopoverTarget(el) {
  while (el) {
      if (el.matches("[data-popover-target]")) {
          return el;
      }
      else {
          el = el.parentElement;
      }
  }

  return null;
}

var Popover = function () {

  function Popover(trigger, _ref) {
      
      var _this = this;

      var _refPosition = _ref.position;
      var position = _refPosition ? _refPosition : "bottom";

      this.trigger = trigger;
      this.position = position;

      var popoverTemplate = document.querySelector("[data-popover=".concat(trigger.dataset.popoverTarget, "]"));
      if (popoverTemplate.tagName.toLowerCase() == "template")
      {
          this.popover = document.createElement("aside");
          this.popover.innerHTML = popoverTemplate.innerHTML;
          this.popover.classList.add("popover");
      }
      else
      {
          this.popover = popoverTemplate;
          this.popover.classList.add("popover");
          this.popover.classList.remove("popover-content");
      }
      
      // set a11y properties
      if (trigger.getAttribute("data-popover-role") == "dialog")
      {
          this.popover.setAttribute("role", "dialog"); 
          this.popover.setAttribute("aria-modal", "true"); 

          var templateLabelledBy = popoverTemplate.getAttribute("aria-labelledby");
          if (templateLabelledBy)
          {
              this.popover.setAttribute("aria-labelledby", templateLabelledBy);
          }

          var templateLabel = popoverTemplate.getAttribute("aria-label");
          if (templateLabel) {
              this.popover.setAttribute("aria-label", templateLabel);
          }
      }
      else
      {
          this.popover.setAttribute("role", "tooltip");
          thisRandomId = "popover" + Math.random().toString().replace(".", "");
          this.popover.setAttribute("id", thisRandomId);
          this.trigger.setAttribute("aria-describedby", thisRandomId);
      }

      // skin 
      if (trigger.getAttribute("data-popover-skin") == "full-bleed")
      {
          this.popover.classList.add("popover-skin-full-bleed");
      }

      this.handleWindowEvent = function () {
          if (_this.isVisible) {
              var triggerRect = trigger.getBoundingClientRect();
              if(triggerRect.top >= 0 && triggerRect.bottom <= window.innerHeight) {
                  _this.show();
              } else {
                  _this.hide();
              }
          }
      };

      this.handleDocumentEvent = function (evt) {
          var thisTarget = evt.target;
          if (_this.isVisible && thisTarget !== _this.trigger && thisTarget !== _this.popover) {

              if (_findClosestPopover(thisTarget) !== _this.popover || thisTarget.classList.contains("popover-close")) {
                  _this.popover.style.display = "none";
              }
          } 
      };

      this.handleKeydownEvent = function (evt) {
          if (evt.key === "Escape") {
              _this.popover.style.display = "none";
          }
      };
  }

  _createClass(Popover, [{
      key: "show",
      value: function show() {
          document.addEventListener("click", this.handleDocumentEvent);
          window.addEventListener("scroll", this.handleWindowEvent);
          window.addEventListener("resize", this.handleWindowEvent);
          document.addEventListener("keydown", this.handleKeydownEvent);

          document.body.appendChild(this.popover);
          this.popover.style.display = "block";

          var thisTriggerBounding = this.trigger.getBoundingClientRect(),
              triggerTop = thisTriggerBounding.top,
              triggerLeft = thisTriggerBounding.left;

          var thisTrigger = this.trigger,
              triggerHeight = thisTrigger.offsetHeight,
              triggerWidth = thisTrigger.offsetWidth;

          var thisPopover = this.popover,
              popoverHeight = thisPopover.offsetHeight,
              popoverWidth = thisPopover.offsetWidth;

          var styleTop = 0;
          var styleLeft = 0;

          switch (this.position) {
              case "top":
                  styleTop = triggerTop - popoverHeight;
                  styleLeft = triggerLeft - (popoverWidth - triggerWidth) / 2;
                  break;
              case "left":
                  styleTop = triggerTop - (popoverHeight - triggerHeight) / 2;
                  styleLeft = triggerLeft - popoverWidth;
                  break;
              case "right":
                  styleTop = triggerTop - (popoverHeight - triggerHeight) / 2;
                  styleLeft = triggerLeft + triggerWidth;
                  break;
              default: // bottom
                  styleTop = triggerTop + triggerHeight;
                  styleLeft = triggerLeft - (popoverWidth - triggerWidth) / 2;
                  break;
          }

          thisPopover.style.top = styleTop + "px";
          thisPopover.style.left = styleLeft + "px";

          thisPopover.classList.remove("popover-position-left");
          thisPopover.classList.remove("popover-position-right");
          thisPopover.classList.remove("popover-position-top");
          thisPopover.classList.remove("popover-position-bottom");
          thisPopover.classList.remove("popover-hide-arrow");

          thisPopover.classList.add("popover-position-" + this.position);

          // if the popover is not fully in the viewport, reposition it
          // also remove the arrowhead, so that it's not pointing to the wrong thing
          var rect = thisPopover.getBoundingClientRect();
          var html = document.documentElement;

          if (rect.top < 0)
          {
              thisPopover.style.top = 0;
              thisPopover.style.marginTop = 0;
              thisPopover.classList.add("popover-hide-arrow");
          }
          if (rect.left < 0)
          {
              thisPopover.style.left = 0;
              thisPopover.style.marginLeft = 0;
              thisPopover.classList.add("popover-hide-arrow");
          }
          if (rect.right >= (window.innerWidth || html.clientWidth))
          {
              thisPopover.style.left = ((window.innerWidth || html.clientWidth) - popoverWidth) + "px";
              thisPopover.style.marginLeft = 0;
              thisPopover.classList.add("popover-hide-arrow");
          }
          if (rect.bottom >= (window.innerHeight || html.clientHeight))
          {
              thisPopover.style.top = ((window.innerHeight || html.clientHeight) - popoverHeight) + "px";
              thisPopover.style.marginTop = 0;
              thisPopover.classList.add("popover-hide-arrow");
          }
      }
  }, {
      key: "hide",
      value: function hide() {
          this.popover.style.display = "none";
          document.removeEventListener("click", this.handleDocumentEvent);
          window.removeEventListener("scroll", this.handleWindowEvent);
          window.removeEventListener("resize", this.handleWindowEvent);
          document.removeEventListener("keydown", this.handleKeydownEvent);
      }
  }, {
      key: "toggle",
      value: function toggle() {
          if (this.isVisible) {
              this.hide();
          } else {
              this.show();
          }
      }
  }, {
      key: "isVisible",
      get: function get() {
          return (document.body.contains(this.popover) && (this.popover.style.display == "block"));
      }
  }]);

  return Popover;
}();

document.addEventListener("click", function (event) {

  var clickedElement = event.target;
  var launchesPopover = _findClosestPopoverTarget(clickedElement);

  if (launchesPopover) {

      var hasCreatedPopover = launchesPopover.getAttribute("data-popover-created");
      if (hasCreatedPopover !== "true") {
          launchesPopover.setAttribute("data-popover-created", "true");

          var myPosition = "bottom";
          var myLauncherPosition = launchesPopover.getAttribute("data-popover-position");
          if (myLauncherPosition)
          {
              myPosition = myLauncherPosition;
          }

          var myPopover = new Popover(launchesPopover, {
              position: myPosition
          });

          launchesPopover.addEventListener("click", function () {
              myPopover.toggle();
          });
          
          // if the launcher has any child elements, they need to also toggle the popover
          var launcherChildren = launchesPopover.children;
          Array.prototype.forEach.call(launcherChildren, function (thisChild) {    
              thisChild.addEventListener("click", function (evt) {
                  evt.stopPropagation(); // prevent the event from bubbling up to the parent, thus toggling the popover twice
                  myPopover.toggle(); 
              });
          });

          return myPopover.toggle();
      } 
  }
});

export default ""